#!/bin/sh

DOCKER_CONTAINER_NAME_PREFIX=alfmarkdown

if [ -z "${M2_HOME}" ]; then
  export MVN_EXEC="mvn"
else
  export MVN_EXEC="${M2_HOME}/bin/mvn"
fi

start() {
	$MVN_EXEC clean package
	$MVN_EXEC -pl $1 integration-test
}

stop() {
	docker container stop $(docker container ls -q --filter name=$DOCKER_CONTAINER_NAME_PREFIX)
	docker container rm $(docker container ls -aq --filter name=$DOCKER_CONTAINER_NAME_PREFIX)
}

reload() {
	docker container stop $(docker container ls -q --filter name=$DOCKER_CONTAINER_NAME_PREFIX-.*-$1)
	docker container start $(docker container ls -q --filter name=$DOCKER_CONTAINER_NAME_PREFIX-.*-$1)
}

list() {
	docker container ls --filter name=$DOCKER_CONTAINER_NAME_PREFIX
}

tail() {
	docker container logs $2 $(docker container ls -q --filter name=$DOCKER_CONTAINER_NAME_PREFIX-.*-$1)
}

case "$1" in
	status)
		list
		;;
	start)
		if [ -z "$2" ]; then
			echo "Usage: $0 start <project_folder>"
			$MVN_EXEC -q --also-make exec:exec -Dexec.executable="pwd" | sed 's~/mnt/data/home/brian/Code/git/alfresco/alfmarkdown/\?\(.*\)~\1~'
		else
			start $2
		fi
		;;
	stop)
		stop
		;;
	reload)
		if [ -z "$2" ]; then
			echo "Usage: $0 restart <container_suffix>"
			docker container ls --filter name=$DOCKER_CONTAINER_NANE_PREFIX --format "{{.Names}}"
		else
			restart $2
		fi
		;;
	follow)
		if [ -z "$2" ]; then
			echo "Usage: $0 follow <container_suffix>"
			docker container ls --filter name=$DOCKER_CONTAINER_NANE_PREFIX --format "{{.Names}}"
		else
			tail $2 "-f"
		fi
		;;
	tail)
		if [ -z "$2" ]; then
			echo "Usage: $0 tail <container_suffix> [ since ]"
			echo "  Example since: 40m (for 40 minutes into history)"
			docker container ls --filter name=$DOCKER_CONTAINER_NANE_PREFIX --format "{{.Names}}"
		elif [ -z "$3" ]; then
			tail $2 "--tail 20"
		else
			tail $2 "--since $3"
		fi
		;;
	*)
		echo "Usage: $0 { status | start <project_folder> | stop | reload <container_suffix> | follow <container_suffix> | tail <container_suffix> [ since ] }"
esac

