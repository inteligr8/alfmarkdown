(function() {

    Alfresco.WebPreview.prototype.Plugins.MarkDown = function(wp, attributes)
    {
        this.wp = wp;
        this.attributes = YAHOO.lang.merge(Alfresco.util.deepCopy(this.attributes), attributes);
        return this;
    };

    Alfresco.WebPreview.prototype.Plugins.MarkDown.prototype =
    {
        attributes: {},

        report: function() {
            return null;
        },

        display: function() {

            var node = this.attributes.node;

            //get the default relative path of the node
            var locationPath = Alfresco.constants.PROXY_URI_RELATIVE + "/markdown" + this.attributes.location.repoPath + "/";

            //get the Div Element we'll be putting the Markdown
            var divElem = document.getElementById(this.wp.id + "-body")

            //Execute Ajax request for content
            require(["dojo/request", "markdown-it", "markdown-it-ins", "markdown-it-sub", "markdown-it-sup", "markdown-it-emoji", "markdown-it-footnote", "markdown-it-deflist"],
            		function(request, MarkdownIt,   MarkdownItIns,     MarkdownItSub,     MarkdownItSuper,   MarkdownItEmoji,     MarkdownItFootnote,     MarkdownItDefList){
            	
            	var mdOpts = {
            		html: true,
            		typographer: true,
            		highlight: function (str, lang) {
					    if (lang && hljs.getLanguage(lang)) {
					        try {
					            return hljs.highlight(lang, str).value;
					        } catch (__) {}
					    }
					
					    return ''; // use external default escaping
					}
				};
				
            	var md = new MarkdownIt(mdOpts)
						.use(new MarkdownItIns())
						.use(new MarkdownItSub())
						.use(new MarkdownItSuper())
						.use(new MarkdownItEmoji())
						.use(new MarkdownItFootnote())
						.use(new MarkdownItDefList());

                var translateImageSrc = function(source) {
                                    return source.replace(/<img src="([^"]*)"/g, function(match, src) {

                                        if(src.startsWith("http")) {
                                            //if this includes external links, then don't change it.
                                            return match;
                                        } else {
                                            //if it's a relative link, we need to use our webscript
                                            return "<img src=\"" + locationPath + src + "\"";
                                        }
                                    });
                };


                request.get(Alfresco.constants.PROXY_URI_RELATIVE + node.contentURL).then(function(mdData) {


                    newHtml = md.render(mdData);

                    divElem.className = "markdown-body";
                    divElem.innerHTML = newHtml;


                });

            });

        }
    }

})();
