package com.inteligr8.alfresco.module.alfmarkdown;

import org.alfresco.transformer.executors.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class FlexmarkTransformerTest extends TransformerTest {

	@Autowired
	@Qualifier(FlexmarkConfig.ID)
	protected Transformer transformer;
	
	@Override
	public Transformer getTransformer() {
		return this.transformer;
	}
	
}
