package com.inteligr8.alfresco.module.alfmarkdown;

import org.alfresco.transformer.AbstractTransformerController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.inteligr8.junit4.AssertRegex;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TransformerController.class)
public class TransformerControllerTest {

	@Autowired
	protected AbstractTransformerController controller;
	
	@Test
	public void tryTransformerName() {
		Assert.assertEquals("alfmarkdown", this.controller.getTransformerName());
	}
	
	@Test
	public void tryVersion() {
		AssertRegex.assertMatches("[0-9]+\\.[0-9]+(\\.[0-9]+|-SNAPSHOT)", this.controller.version());
	}
	
}
