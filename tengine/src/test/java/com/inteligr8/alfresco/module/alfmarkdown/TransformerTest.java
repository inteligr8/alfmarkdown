package com.inteligr8.alfresco.module.alfmarkdown;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.alfresco.transform.exceptions.TransformException;
import org.alfresco.transformer.executors.Transformer;
import org.jsoup.Jsoup;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.inteligr8.junit4.AssertRegex;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TransformerController.class)
public abstract class TransformerTest {
	
	private final Logger logger = LoggerFactory.getLogger(TransformerTest.class);
	
	public abstract Transformer getTransformer();
	
	@Test
	public void tryTransformerId() {
		AssertRegex.assertMatches("flexmark|commonmark", this.getTransformer().getTransformerId());
	}
	
	@Test(expected = TransformException.class)
	public void tryExtractMetadata() throws Exception {
		this.getTransformer().extractMetadata("does-not-matter", "does-not-matter", "does-not-matter", Collections.emptyMap(), null, null);
	}
	
	@Test
	public void tryTransformUnsupportedMedia() throws Exception {
		List<String[]> mediaTypePairs = Arrays.asList(
			new String[] {MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_PLAIN_VALUE},
			new String[] {MediaType.TEXT_PLAIN_VALUE, MediaType.TEXT_MARKDOWN_VALUE},
			new String[] {MediaType.TEXT_MARKDOWN_VALUE, MediaType.TEXT_PLAIN_VALUE},
			new String[] {MediaType.TEXT_HTML_VALUE, MediaType.TEXT_MARKDOWN_VALUE},
			new String[] {MediaType.TEXT_MARKDOWN_VALUE, MediaType.TEXT_MARKDOWN_VALUE}
		);
		
		for (String[] mediaTypePair : mediaTypePairs) {
			try {
				this.getTransformer().transform(mediaTypePair[0], mediaTypePair[1], Collections.emptyMap(), null, null);
				Assert.fail();
			} catch (TransformException te) {
				try {
					Assert.assertEquals(te.getMessage(), HttpStatus.BAD_REQUEST.value(), te.getStatusCode());
				} catch (AssertionError ae) {
					this.logger.error(ae.getMessage(), te);
					throw ae;
				}
			}
		}
	}
	
	@Test
	public void tryTransformQuick() throws IOException, URISyntaxException {
		File targetFile = this.transform(Collections.emptyMap(), new File(this.getClass().getResource("/quick.md").toURI()));
		try {
			Assert.assertEquals(70L, targetFile.length());
			
			// is valid html
			Jsoup.parse(targetFile, "utf-8");
		} finally {
			targetFile.delete();
		}
	}
	
	private File transform(Map<String, String> transformOptions, File sourceFile) throws IOException, TransformException {
		File htmlFile = File.createTempFile("test-", ".html");
		try {
			this.getTransformer().transform(MediaType.TEXT_MARKDOWN_VALUE, MediaType.TEXT_HTML_VALUE, transformOptions, sourceFile, htmlFile);
			return htmlFile;
		} catch (RuntimeException re) {
			htmlFile.delete();
			throw re;
		} catch (Error e) {
			htmlFile.delete();
			throw e;
		}
	}
	
}
