/*
 * #%L
 * Alfresco Transform Core
 * %%
 * Copyright (C) 2005 - 2020 Alfresco Software Limited
 * %%
 * This file is part of the Alfresco software.
 * -
 * If the software was purchased under a paid Alfresco license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 * -
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * -
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * -
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 * 
 * Copyright (C) 2020 - 2021 Inteligr8
 */
package com.inteligr8.alfresco.module.alfmarkdown;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.alfresco.transform.exceptions.TransformException;
import org.alfresco.transformer.AbstractTransformerController;
import org.alfresco.transformer.executors.Transformer;
import org.alfresco.transformer.probes.ProbeTestTransform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;

/**
 * Controller for the Spring Boot transformer.
 *
 * Status Codes:
 *
 * 200 Success
 * 400 Bad Request: Request parameter <name> is missing (missing mandatory parameter)
 * 400 Bad Request: Request parameter <name> is of the wrong type
 * 400 Bad Request: Transformer exit code was not 0 (possible problem with the source file)
 * 400 Bad Request: The source filename was not supplied
 * 500 Internal Server Error: (no message with low level IO problems)
 * 500 Internal Server Error: The target filename was not supplied (should not happen as targetExtension is checked)
 * 500 Internal Server Error: Transformer version check exit code was not 0
 * 500 Internal Server Error: Transformer version check failed to create any output
 * 500 Internal Server Error: Could not read the target file
 * 500 Internal Server Error: The target filename was malformed (should not happen because of other checks)
 * 500 Internal Server Error: Transformer failed to create an output file (the exit code was 0, so there should be some content)
 * 500 Internal Server Error: Filename encoding error
 * 507 Insufficient Storage: Failed to store the source file
 */
@Controller
public class TransformerController extends AbstractTransformerController {
	
	private final Logger logger = LoggerFactory.getLogger(TransformerController.class);
	private final Pattern fileext = Pattern.compile("\\.([^\\.]+)$");
	private final MediaType defaultTarget = MediaType.TEXT_HTML;

	@Autowired
	private List<Transformer> transformers;
	
	@Value("${transform.alfmarkdown.version}")
	private String version;
	
	@Value("${transform.alfmarkdown.defaultEngine}")
	private String defaultEngine;
	
	private Map<String, Transformer> engineMap;
	private ProbeTestTransform probe;

	@Override
	public String getTransformerName() {
		return "alfmarkdown";
	}

	@Override
	public String version() {
		return this.version;
	}
	
	@PostConstruct
	public void registerEngines() {
		this.engineMap = new HashMap<>();
		for (Transformer transformer : this.transformers) {
			if (this.logger.isInfoEnabled())
				this.logger.info("Registered transformer/engine: " + transformer.getTransformerId());
			this.engineMap.put(transformer.getTransformerId(), transformer);
		}
	}
	
	@PostConstruct
	public void initProbe() {
		this.probe = new ProbeTestTransform(this, "quick.md", "quick.html", 
					500L, 500L, 150, 16384L, 10L, 300L) {
			@Override
			protected void executeTransformCommand(File sourceFile, File targetFile) {
				if (logger.isTraceEnabled())
					logger.trace("getProbeTestTransform().executeTransformCommand('" + sourceFile + "', '" + targetFile + "')");
				Transformer transformer = transformers.get(new Random().nextInt(2));
				transformer.transform(MediaType.TEXT_MARKDOWN_VALUE, MediaType.TEXT_HTML_VALUE, Collections.emptyMap(), sourceFile, targetFile);
			}
		};
	}

	@Override
	public ProbeTestTransform getProbeTestTransform() {
		if (this.logger.isTraceEnabled())
			this.logger.trace("getProbeTestTransform()");
		return this.probe;
	}

	@Override
	protected String getTransformerName(final File sourceFile, final String sourceMimetype, final String targetMimetype, final Map<String, String> transformOptions) {
		if (this.logger.isTraceEnabled())
			this.logger.trace("getTransformerName('" + sourceFile + "', '" + sourceMimetype + "', '" + targetMimetype + "', " + transformOptions + ")");
		// does not matter what value is returned, as it is not used because there is only one.
		return this.getTransformerName();
	}

	@Override
	public void transformImpl(String transformName, String sourceMimetype, String targetMimetype,  Map<String, String> transformOptions, File sourceFile, File targetFile) {
		if (this.logger.isTraceEnabled())
			this.logger.trace("transformImpl('" + transformName + "', '" + sourceMimetype + "', '" + targetMimetype + "', " + transformOptions.keySet() + ", '" + sourceFile + "', '" + targetFile + "')");
		
		if (sourceMimetype == null) {
			Matcher matcher = this.fileext.matcher(sourceFile.getAbsolutePath());
			sourceMimetype = matcher.find() ? this.ext2mime(matcher.group(1)) : null;
		}
		if (targetMimetype == null) {
			Matcher matcher = this.fileext.matcher(targetFile.getAbsolutePath());
			targetMimetype = matcher.find() ? this.ext2mime(matcher.group(1)) : this.defaultTarget.toString();
		}
		
		String engine = transformOptions.getOrDefault(RequestParamConstants.ENGINE, this.defaultEngine);
		if (!this.engineMap.containsKey(engine))
			throw new TransformException(HttpStatus.BAD_REQUEST.value(), "This transformer does not support the following engine: " + engine);
		
		try {
			this.engineMap.get(engine).transform(transformName, sourceMimetype, targetMimetype, transformOptions, sourceFile, targetFile);
		} catch (Exception e) {
			this.logger.error("The transformation encountered an unexpected issue", e);
			throw new TransformException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "The transformer encountered an unexpected");
		}
	}
	
	private String ext2mime(String ext) {
		switch (ext.toLowerCase()) {
			// add applicable extensions here
			case "md":
			case "markdown": return MediaType.TEXT_MARKDOWN_VALUE;
			case "html":
			case "htm": return MediaType.TEXT_HTML_VALUE;
			case "text":
			case "txt": return MediaType.TEXT_PLAIN_VALUE;
			case "pdf": return MediaType.APPLICATION_PDF_VALUE;
			default: return null;
		}
	}
}
