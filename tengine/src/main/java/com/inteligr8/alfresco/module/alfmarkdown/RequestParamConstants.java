package com.inteligr8.alfresco.module.alfmarkdown;

public interface RequestParamConstants {

	public static final String ENGINE = "engine";
	public static final String PROFILE = "profile";
	public static final String EXT_CLASS_NAMES = "extensions";

}
