package com.inteligr8.alfresco.module.alfmarkdown;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.alfresco.transform.exceptions.TransformException;
import org.alfresco.transformer.executors.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.parser.ParserEmulationProfile;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.IRender;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.vladsch.flexmark.util.misc.Extension;

@Component(FlexmarkConfig.ID)
public class FlexmarkTransformer implements Transformer {
	
	private final Logger logger = LoggerFactory.getLogger(FlexmarkTransformer.class);
	private final List<String> classSearchPrefixes = Arrays.asList("", "com.vladsch.flexmark.ext.{name}.", "com.vladsch.flexmark.ext.");
	private final Pattern extClassNamePattern = Pattern.compile("[\\.]?(([A-Za-z0-9]+)Extension|[A-Za-z0-9]+)$");
	
	@Autowired
	private FlexmarkConfig config;
	
	@PostConstruct
	public void init() throws Exception {
		if (this.logger.isDebugEnabled())
			this.logger.debug("init()");
	}
	
	@Override
	public String getTransformerId() {
		return FlexmarkConfig.ID;
	}
	
	@Override
	public void extractMetadata(String transformName, String sourceMimetype, String targetMimetype, Map<String, String> transformOptions, File sourceFile, File targetFile)
	throws IOException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("extractMetadata('" + transformName + "', '" + sourceMimetype + "', '" + targetMimetype + "', " + transformOptions.keySet() + ", '" + sourceFile + "', '" + targetFile + "')");
		throw new TransformException(HttpStatus.NOT_IMPLEMENTED.value(), "This transformer does not support meta-data extraction");
	}
	
	@Override
	public void transform(String transformName, String sourceMimetype, String targetMimetype, Map<String, String> transformOptions, File sourceFile, File targetFile)
	throws IOException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("transform('" + transformName + "', '" + sourceMimetype + "', '" + targetMimetype + "', " + transformOptions.keySet() + ", '" + sourceFile + "', '" + targetFile + "')");
		if (!MediaType.TEXT_MARKDOWN_VALUE.equals(sourceMimetype))
			throw new TransformException(HttpStatus.BAD_REQUEST.value(), "This transformer does not support source data of the '" + sourceMimetype + "' type");

		String profile = transformOptions.getOrDefault(RequestParamConstants.PROFILE, this.config.getDefaultProfile());
		
		MutableDataSet options = new MutableDataSet();
		options.setFrom(ParserEmulationProfile.valueOf(profile.toUpperCase()));
		options.set(Parser.EXTENSIONS, this.gatherExtensions(transformOptions));
		
		Parser parser = Parser.builder(options).build();
		IRender renderer;
		
		switch (targetMimetype) {
			case MediaType.TEXT_HTML_VALUE:
				renderer = HtmlRenderer.builder(options).build();
				break;
			default:
				throw new TransformException(HttpStatus.BAD_REQUEST.value(), "This transformer does not support target data of the '" + targetMimetype + "' type");
		}
		
		FileWriter fwriter = new FileWriter(targetFile, false);
		try {
			FileReader freader = new FileReader(sourceFile);
			try {
				Document mddoc = parser.parseReader(freader);
				renderer.render(mddoc, fwriter);
			} finally {
				freader.close();
			}
		} finally {
			fwriter.close();
		}
	}
	
	private List<Extension> gatherExtensions(Map<String, String> transformOptions) {
		// include default extensions
		Set<String> extClassNames = new HashSet<>();
		if (this.config.getDefaultExtensions() != null)
			extClassNames.addAll(this.config.getDefaultExtensions());
		
		// include/exclude based on passed in extensions
		String extClassNamesRaw = transformOptions.get(RequestParamConstants.EXT_CLASS_NAMES);
		if (extClassNamesRaw != null) {
			for (String extClassName : extClassNamesRaw.split("[,\\n]")) {
				extClassName = extClassName.trim();
				if (extClassName.length() > 0) {
					if (extClassName.startsWith("!")) {
						// exclude those that start with !
						extClassNames.remove(extClassName.substring(1));
					} else {
						// include the rest
						extClassNames.add(extClassName);
					}
				}
			}
		}
		
		List<Extension> exts = new ArrayList<>(extClassNames.size());
		
		// create the extension classes using reflection
		for (String extClassName : extClassNames) {
			Class<?> extClass = this.findClass(extClassName);
			if (extClass != null) {
				Extension ext = this.createExtension(extClass);
				if (ext != null)
					exts.add(ext);
			}
		}
		
		return exts;
	}
	
	private Class<?> findClass(String className) {
		Matcher matcher = this.extClassNamePattern.matcher(className);
		String extName = matcher.find() ? matcher.group(2).toLowerCase() : null;
		
		for (String classPrefix : this.classSearchPrefixes) {
			if (extName != null)
				classPrefix = classPrefix.replaceAll("\\{name\\}", extName);
			
			try {
				Class<?> extClass = Class.forName(classPrefix + className);
				return extClass;
			} catch (ClassNotFoundException cnfe) {
				// suppress
			}
		}
		
		return null;
	}
	
	private Extension createExtension(Class<?> extClass) {
		try {
			// finding public static method, so using "DeclaredMethod" instead of "Method"
			Method createMethod = extClass.getDeclaredMethod("create");
			// calling public static method, so no instance (null)
			return (Extension)createMethod.invoke(null);
		} catch (NoSuchMethodException nsme) {
			this.logger.warn("The '" + extClass.getName() + "' extension class must have a public static 'create' method");
		} catch (InvocationTargetException ite) {
			this.logger.warn("The '" + extClass.getName() + "' extension class 'create' method encountered an unexpected exception: " + ite.getMessage());
		} catch (IllegalAccessException iae) {
			this.logger.warn("The '" + extClass.getName() + "' extension class 'create' method is not public: " + iae.getMessage());
		}

		return null;
	}

}
