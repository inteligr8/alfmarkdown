package com.inteligr8.alfresco.module.alfmarkdown;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "transform.alfmarkdown." + FlexmarkConfig.ID)
public class FlexmarkConfig {
	
	static final String ID = "flexmark";
	
	private String defaultProfile;
	private List<String> defaultExtensions;
	
	public String getDefaultProfile() {
		return this.defaultProfile;
	}
	
	void setDefaultProfile(String defaultProfile) {
		this.defaultProfile = defaultProfile;
	}
	
	public List<String> getDefaultExtensions() {
		return this.defaultExtensions;
	}
	
	void setDefaultExtensions(List<String> defaultExtensions) {
		this.defaultExtensions = defaultExtensions;
	}

}
