package com.inteligr8.alfresco.module.alfmarkdown;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "transform.alfmarkdown")
public class Config {
	
	private FlexmarkConfig flexmark;
	private CommonmarkConfig commonmark;
	
	public FlexmarkConfig getFlexmark() {
		return this.flexmark;
	}
	
	void setFlexmark(FlexmarkConfig flexmark) {
		this.flexmark = flexmark;
	}
	
	public CommonmarkConfig getCommonmark() {
		return this.commonmark;
	}
	
	void setCommonmark(CommonmarkConfig commonmark) {
		this.commonmark = commonmark;
	}

}
