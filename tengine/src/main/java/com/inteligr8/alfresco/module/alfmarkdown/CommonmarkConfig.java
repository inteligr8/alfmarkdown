package com.inteligr8.alfresco.module.alfmarkdown;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "transform.alfmarkdown." + CommonmarkConfig.ID)
public class CommonmarkConfig {
	
	static final String ID = "commonmark";
	
	private List<String> defaultExtensions;
	
	public List<String> getDefaultExtensions() {
		return this.defaultExtensions;
	}
	
	void setDefaultExtensions(List<String> defaultExtensions) {
		this.defaultExtensions = defaultExtensions;
	}

}
